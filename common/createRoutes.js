const { $content } = require("@nuxt/content");

async function createRoutes() {
  
    const files = await $content("blog", { deep: true }).only(["path"]).fetch();
    return files.map((file) => (file.path.replace('/blog/', '/garden/')));
    
}

module.exports = createRoutes;