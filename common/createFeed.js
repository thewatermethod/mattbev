const { $content } = require('@nuxt/content');

module.exports = async function (feed) {

    feed.options = {
        title: 'matt bevilacqua | writings',
        link: 'https://www.mattbev.com/feed.xml',
        description: 'Writings and Links about Web Development, Accessibility, Horror Movies, and Gaming'
    };   

    // get all the posts we have
    const posts = await $content('blog').fetch(); 
        
    posts.forEach(async post => {

        const url = `https://www.mattbev.com/garden/${post.slug}`;      
          
        feed.addItem({
            title: post.title,
            id: url,
            link: url,
            description: post.excerpt ? post.excerpt : post.bodyPlainText.substr(0,100),
            content: post.bodyPlainText
        }); 
 
    });     
}