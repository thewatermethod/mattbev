function formatDate(rawDateString) { 

    const months = [
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December"
    ]

    const date = new Date(rawDateString);
    const month = date.getMonth();
    const year = date.getFullYear();
    const day = date.getDate(); 


    return `${day} ${months[month]} ${year}`;
}

export { formatDate };