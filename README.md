# mattbev

> Nuxt.js project

## Build Setup

```bash
# install dependencies
$ npm install # Or yarn install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, checkout the [Nuxt.js docs](https://github.com/nuxt/nuxt.js).

[![Netlify Status](https://api.netlify.com/api/v1/badges/9e44eb40-56f4-4d52-bbb9-cca2f2769d31/deploy-status)](https://app.netlify.com/sites/thirsty-hawking-dfa84a/deploys)
