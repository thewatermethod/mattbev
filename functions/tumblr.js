const fetch = require('node-fetch');

exports.handler = async (event, context) => {
 
    let id_string = '';
    const id = event.queryStringParameters && event.queryStringParameters.id;

    if (id) { 
        id_string = `&id=${id}`;
    }

    const url = `https://api.tumblr.com/v2/blog/thewatermethod.tumblr.com/posts?api_key=${process.env.TUMBLR_API_KEY}${id_string}&filter=html&notes_info=true&reblog_info=true`;
    const res = await fetch(url);
    const data = await res.json();

    const posts = data.response.posts

    return {
        body: JSON.stringify(posts),
        statusCode: 200
    }
 
}

