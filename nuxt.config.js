// const fetch = require('node-fetch');
const createRoutes = require("./common/createRoutes");
const createFeed = require('./common/createFeed');

module.exports = {

  css: ["@/assets/style.css"],

  target: "static",

  /**
   * 1) nuxt content is a very useful plugin for managing static content
   * 
   * 2) nuxt sitemap does what it sounds like it does **NOTE** according to docs, this should be loaded last
   * 
   */

  modules: ["@nuxt/content", "@nuxtjs/feed", "@nuxtjs/sitemap"],

  hooks: {
    'content:file:beforeInsert': (document) => {
      if (document.extension === '.md') {      
        document.bodyPlainText = document.text;
      }
    },
  },


  content: {
    dir: "_posts",
    markdown: {
      prism: {
        theme: 'prism-themes/themes/prism-synthwave84.css'
      }
    }
  },

  feed: [
    {
      path: '/feed.xml', // The route to your feed.
      async create(feed) {
        createFeed(feed)
      },
      cacheTime: 1000 * 60 * 15, // How long should the feed be cached
      type: 'rss2', // Can be: rss2, atom1, json1
      data: ['Some additional data'] // Will be passed as 2nd argument to `create` function
    }
  ],

  /*
   ** HTML head
   */

  head: {
    title: "mattbev",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "Nuxt.js project" }
    ],
    link: [
      { rel: "icon", type: "image/x-icon", href: "/favicon.ico" },
      {
        href:
          "https://fonts.googleapis.com/css2?family=Nanum+Gothic+Coding&display=swap",
        rel: "stylesheet"
      }
    ],

    htmlAttrs: {
      lang: "en-us" // it sets the language English
    },


  },
  /*
   ** Customize the progress bar color
   */
  loading: { color: "#3B8070" },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** Run ESLint on save
     */
  },

  sitemap: {
    hostname: 'https://www.mattbev.com',
    routes() {
      return createRoutes()
    }
  },

  generate: {
    exclude: [
      /^\/tumblr/ // path starts with /admin
    ],
    // async routes() {
    //   const res = await fetch('https://mattbev.com/.netlify/functions/tumblr');
    //   const json = await res.json();

    //   return json.map(post => {

    //     console.log(post);

    //     return {
    //       route: '/tumblr/' + post.id_string,
    //       payload: post
    //     }
    //   });
    // }
  }
};



