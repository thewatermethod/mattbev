---
title: Stony the Road - Reconstruction, White Supremacy, and the Rise of Jim Crow
author: Henry Louis Gates Jr.
thumbnail: /static/images/stony-the-road.jpg
amazon: https://amzn.to/2KK9Rs7
read: 2019-11-22T20:48:25.371Z
---
